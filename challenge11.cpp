#include <iostream>

using namespace std;

template <class T>
void swap_values(T&, T&);
template <class T>
void sort(T a[], int);	//sort index
template <class T>
int index_of_smallest(const T a[], int, int);	//find the smallest index

template <class T>
void swap_values(T& variable1, T& variable2)
{
	T temp;
	temp = variable1; //the first variable is copied into the temp.
	variable1 = variable2; //the second variable is copied to the first variable.
	variable2 = temp; //the temp variable is copied back to the second variable,
}

template <class T>

void sort(T a[], int number_used)
{
	int index_of_next_smallest;
	for (int index = 0; index < number_used-1; index++)
	{
		index_of_next_smallest = index_of_smallest(a, index, number_used);
		swap_values(a[index], a[index_of_next_smallest]);
	}
}

template <class T>

int index_of_smallest(const T a[], int start_index, int number_used)	//find smallest index
{
	T min = a[start_index];
	int index_of_min = start_index;
	for (int index = start_index + 1; index < number_used; index++)
	if (a[index] < min)
	{
		min = a[index];
		index_of_min = index;
	}
	return index_of_min;
}

int main()
{
	int array[10] = {1,4,6,7,3,2,9,10,5,8};
	sort(array,10);
	for (int i=0; i<10; i++)
	{
		cout << array[i] << " ";
	}
	
	system("pause");
	return 0;
}
